SELECT 
PurchaseDetailItemAutoId, 
PurchaseOrderNumber, 
ROW_NUMBER() OVER (PARTITION BY PurchaseOrderNumber, ItemNumber ORDER BY PurchaseDetailItemAutoId) AS LineNumber,
ItemNumber, 
ItemName, 
ItemDescription, 
PurchasePrice, 
PurchaseQuantity, 
LastModifiedByUser, 
LastModifiedDateTime
FROM PurchaseDb.dbo.PurchaseDetailItem