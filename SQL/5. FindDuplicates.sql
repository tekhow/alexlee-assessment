SELECT PurchaseOrderNumber,
    ItemNumber,
    PurchasePrice,
    PurchaseQuantity,
    COUNT(*)
FROM PurchaseDb.dbo.PurchaseDetailItem
GROUP BY PurchaseOrderNumber,
    ItemNumber,
    PurchasePrice,
    PurchaseQuantity
HAVING COUNT(*) > 1