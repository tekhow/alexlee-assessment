﻿using System.Text;

namespace TestMethods;

public class Methods
{
    /// <summary>
    /// Creates an interleaved string from two strings. The first string is treated as the base of the second string and the second string is used as the alternate characters.
    /// </summary>
    /// <param name="s1">The string to interleave. Must not be null or empty.</param>
    /// <param name="s2">The string to interleave with. Must not be null or empty.</param>
    /// <returns>The interleaved string as a string of characters from s1 and s2.</returns>
    public static string CreateInterleavedString(string s1, string s2)
    {
        // Checks if both strings are null or whitespace
        if (string.IsNullOrWhiteSpace(s1) || string.IsNullOrWhiteSpace(s2))
        {
            throw new Exception("s1 and s2 cannot be null or empty");
        }

        var interleavedChars = new StringBuilder();
        var s2Index = 0;

        // loop through s1 and then get the alternate characters from s2 on each iteration
        foreach (var c in s1)
        {
            interleavedChars.Append(c);

            // Don't increment past s1's length.
            if (s2Index == s2.Length - 1)
            {
                continue;
            }

            interleavedChars.Append(s2[s2Index]);
            s2Index++;
        }

        // write the remaining characters if there are any
        if (s2Index <= s2.Length - 1)
        {
            interleavedChars.Append(s2.Substring(s2Index));
        }

        return interleavedChars.ToString();
    }

    /// <summary>
    /// Checks if a string is a palindrome.
    /// </summary>
    /// <param name="s1">The string to check. Must not be null or empty.</param>
    /// <returns>True if the string is a palindrome, false otherwise.</returns>
    public static string CheckIsStringPalendrome(object input)
    {
        var stringValue = input.ToString() ?? string.Empty;

        var reversed = new string(stringValue.ToCharArray().Reverse().ToArray());

        var isPalendrome = stringValue.Equals(reversed, StringComparison.CurrentCultureIgnoreCase);

        return isPalendrome ? "Palendrome" : "Not Palendrome";
    }
}