﻿using FileSearchConsole;
using FileSearchConsole.Helpers;
using CH = FileSearchConsole.Helpers.ConsoleHelper;

public class Program
{

    public static async Task Main(string[] args)
    {
        /// <summary>
        /// Startup class is used to load the application configuration.
        /// </summary>
        var startup = new Startup();

        /// <summary>
        /// DirectorySearcher class is used to search for files in a directory.
        /// </summary>
        var dirSearcher = new DirectorySearcher(startup.AppSettings.DirectorySearchPath);

        /// <summary>
        /// GetFilesAsync method is used to search for files in the specified directory and returns a list of files.
        /// </summary>

        var foundFiles = await dirSearcher.GetFilesAsync();

        CH.ConsoleTitle();
        CH.WriteGreen($"{foundFiles.Length} files found in {startup.AppSettings.DirectorySearchPath}");

        var fileSearcher = new FileSearcher(startup.AppSettings.SearchTerm, foundFiles);
        var results = await fileSearcher.GetContentAsync();

        CH.WriteYellow(fileSearcher.GetSearchMetrics(results));
    }
}

