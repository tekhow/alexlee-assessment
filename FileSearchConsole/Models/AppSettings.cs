using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileSearchConsole.Models
{
    public class AppSettings
    {
        public string DirectorySearchPath { get; set; } = string.Empty;
        public string SearchTerm { get; set; } = string.Empty;
    }
}