using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileSearchConsole.Models
{
    public record SearchResult(int Line,int TermCount,string FileName,int Thread);

}