using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileSearchConsole.Models;
using Microsoft.Extensions.Configuration;

namespace FileSearchConsole
{
    public class Startup
    {
        public Startup()
        {
            var builder = new ConfigurationBuilder()
                      .SetBasePath(Directory.GetCurrentDirectory())
                      .AddJsonFile("appsettings.json", optional: false);

            IConfiguration config = builder.Build();

            AppSettings = config.GetSection("AppSettings").Get<AppSettings>()??new AppSettings();

        }

        public AppSettings AppSettings { get; private set; }
    }
}