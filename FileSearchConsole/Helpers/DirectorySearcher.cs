public class DirectorySearcher(string directoryPath)
{

    private string DirectoryPath => directoryPath;

    public async Task<FileInfo[]> GetFilesAsync()
    {
        var directory = new DirectoryInfo(DirectoryPath);

        return directory.GetFiles();
    }
}