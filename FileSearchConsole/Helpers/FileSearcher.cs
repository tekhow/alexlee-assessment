using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Text.Unicode;
using System.Threading.Tasks;
using FileSearchConsole.Models;
using CH = FileSearchConsole.Helpers.ConsoleHelper;

namespace FileSearchConsole.Helpers
{
    public class FileSearcher(string SearchTerm, FileInfo[] FilesToSearch)
    {

        public string GetSearchMetrics(SearchResult[] results)
        {
            var output = results.GroupBy(v => v.FileName)
            .Select(s => new
            {
                FileName = s.First().FileName,
                Total = s.Count(),
                Hits = s.Sum(x => x.TermCount),
                Thread=s.First().Thread
            })
            .ToList();

            var sb = new StringBuilder();
            sb.AppendLine($"[{output.Count()}] files searched.");
            sb.AppendLine(new string('-', 100));    
            foreach(var o in output){
                sb.AppendLine($"File name: {o.FileName}\t\tLines Searched: {o.Total}\t\tHits: {o.Hits} hits\t\tThread: {o.Thread}");
            }

            return sb.ToString();
        }

        public Task<SearchResult[]> GetContentAsync()
        {
            return Task.Run<SearchResult[]>(async () =>
            {
                var results = new List<SearchResult>();
                var work = new List<Task<SearchResult[]>>();

                foreach (var file in FilesToSearch)
                {
                    work.Add(ProcessFileAsync(file));
                }

                foreach (var task in work)
                {
                    await task.ContinueWith(result =>
                    {
                        var value = result.Result;
                        results.AddRange(value);

                    });
                }

                Task.WaitAll(work.ToArray());

                return [.. results];
            });
        }

        private async Task<SearchResult[]> ProcessFileAsync(FileInfo file)
        {
            CH.Write($"Processing {file.Name}");
            var results = new List<SearchResult>();
            using (var stream = file.OpenText())
            {
                stream.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
                int lineNo = 0;
                while (stream.BaseStream.Position < stream.BaseStream.Length)
                {
                    lineNo++;

                    var line = await stream.ReadLineAsync() ?? string.Empty;

                    CH.Write(line);

                    int hits = Regex.Matches(line, SearchTerm, RegexOptions.IgnoreCase).Count;

                    results.Add(new SearchResult(lineNo, hits, file.Name,Thread.CurrentThread.ManagedThreadId));
                }
            }

            return [.. results];
        }


    }
}