using System.Data;
using System.Reflection;

namespace FileSearchConsole.Helpers;

public class ConsoleHelper
{

    public static void ConsoleTitle()
    {
        var version = Assembly.GetExecutingAssembly().GetName().Version?.ToString();
        const string title = "File Search Console";
        Console.Title = title;
        Console.WriteLine();
        Console.WriteLine($"\t\t ========-> {title} v{version} <-========");
        Console.WriteLine();
    }

    public static void WriteGreen(string message = "", params object[] obj)
    {
        Console.BackgroundColor = ConsoleColor.Black;
        Console.ForegroundColor = ConsoleColor.Green;
        if (obj != null)
        {
            Console.WriteLine(message, obj);
        }
        else
        {
            Console.WriteLine(message);
        }
        Console.ResetColor();
    }

    public static void WriteYellow(string message = "", params object[] obj)
    {
        Console.BackgroundColor = ConsoleColor.Black;
        Console.ForegroundColor = ConsoleColor.Yellow;
        if (obj != null)
        {
            Console.WriteLine(message, obj);
        }
        else
        {
            Console.WriteLine(message);
        }
        Console.ResetColor();
    }

    public static void Write(string message, params object[] obj)
    {
        if (obj != null)
        {
            Console.WriteLine(message, obj);
        }
        else
        {
            Console.WriteLine(message);
        }
    }

    public static void Write(params object[] obj)
    {
        if (obj != null)
        {
            Console.WriteLine(obj);
        }
        else
        {
            Console.WriteLine();
        }
    }
}
