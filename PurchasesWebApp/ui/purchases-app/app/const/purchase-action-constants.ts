export class PurchaseActionConstants{
    public static ADD_PURCHASE_ACTION = "ADD_PURCHASE_ACTION";
    public static DELETE_PURCHASE_ACTION = "DELETE_PURCHASE_ACTION";
    public static EDIT_PURCHASE_ACTION = "EDIT_PURCHASE_ACTION";
}