import Purchases from './components/purchases';
import "./css/page.css";
import { getPurchaseDetailItems } from "./data/purchase-data";

export default async function Page() {
  const items = await getPurchaseDetailItems();
  return (
    <div>
      <Purchases items={items} />
    </div>
  );
}
