export type PurchaseDetailItem  ={
  purchaseDetailItemAutoId: number;
  purchaseOrderNumber: string;
  lineNumber: number;
  itemNumber: number;
  itemName: string;
  itemDescription: string;
  purchasePrice: number;
  purchaseQuantity: number;
  lastModifiedByUser: string;
  lastModifiedDateTime: Date;
};

export type PurchaseDetailItemNew= {
  purchaseOrderNumber: string ;
  itemNumber: number ;
  itemName: string ;
  itemDescription: string ;
  purchasePrice: number ;
  purchaseQuantity: number ;
};

export type PurchaseDetailItemModified = {
  purchaseDetailItemAutoId: number ;
  purchaseOrderNumber: string ;
  itemNumber: number ;
  itemName: string ;
  itemDescription: string ;
  purchasePrice: number ;
  purchaseQuantity: number ;
};