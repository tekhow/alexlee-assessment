import moment from "moment";

export function formatDateTime(date?: Date): string {
  return moment(date).format("MM/DD/YYYY HH:mm:ss A");
}

export function formatCurrency(amount: number): string {
  let USDollar = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  return USDollar.format(amount);
}
