import { config, library } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
import { faShop, faMoneyBill, faEdit,faTrash,faAdd , faCancel} from "@fortawesome/free-solid-svg-icons";

// See https://github.com/FortAwesome/react-fontawesome#integrating-with-other-tools-and-frameworks
config.autoAddCss = false; // Tell Font Awesome to skip adding the CSS automatically since it's being imported above
library.add(faShop, faMoneyBill, faEdit, faTrash, faAdd, faCancel);
