"use client";

import { PurchaseDetailItem } from "../models/purchase-detail-item";
import React from "react";
import $ from "jquery";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../lib/icons";
import { formatCurrency, formatDateTime } from "../lib/utils";
import { Button, Modal } from "react-bootstrap";
import PurchaseActions from "./purchases-actions";
import { PurchaseActionConstants } from "../const/purchase-action-constants";
import AlertMessage from "./alert-message";
import { getPurchaseDetailItems } from "../data/purchase-data";

export default function Purchases({ items }: { items: PurchaseDetailItem[] }) {
  const [itemsState, setItemsState] = React.useState(items);
  const [show, setShow] = React.useState(false);
  const [actionState, setActionState] = React.useState("");
  const [modalTitleState, setModalTitleState] = React.useState("");
  const [alertState, setAlertState] = React.useState({
    show: false,
    variant: "",
    message: "",
  });

  const [editItemState, setEditItemState] =
    React.useState<PurchaseDetailItem>();
  const handleClose = () => setShow(false);
  const handleShow = (
    action: string,
    title: string,
    item?: PurchaseDetailItem
  ) => {
    setShow(true);
    setActionState(action);
    setEditItemState(item);
    setModalTitleState(title);
  };

  function filterItems(
    dataField: string,
    e: HTMLInputElement,
    loadedItems: PurchaseDetailItem[]
  ) {
    const value = e.value?.toString();
    if (value && value.length > 0) {
      setItemsState(
        loadedItems.filter((f: any) => {
          let fVal = "";

          // find the field value of the object by key
          Object.keys(f).forEach(function (key) {
            if (key === dataField) {
              console.log(key, f[key]);
              fVal = f[key];
            }
            return;
          });

          // filter if the field contains the value
          return fVal.toUpperCase().includes(value.toUpperCase());
        })
      );
    } else {
      setItemsState(loadedItems);
    }
  }

  function resetState(loadedItems: PurchaseDetailItem[]) {
    // Reset the state
    setItemsState(loadedItems);

    // Clear the input field
    $(document).find("input[type=text], textarea").val("");
  }
  return (
    <div className="container-fluid">
      <h1> Purchases </h1>
      <table className="table table-hover ">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">
              <input
                className="form-control border border-primary"
                type="text"
                onChange={(e) => {
                  filterItems("purchaseOrderNumber", e.target, items);
                }}
              />
              Order Number
            </th>
            <th scope="col">Line Number</th>
            <th scope="col">
              <input
                className="form-control border border-primary"
                type="text"
                onChange={(e) => {
                  filterItems("itemNumber", e.target, items);
                }}
              />
              Item Number
            </th>
            <th scope="col">
              <input
                className="form-control border border-primary"
                type="text"
                onChange={(e) => {
                  filterItems("itemName", e.target, items);
                }}
              />
              Item Name
            </th>
            <th scope="col">
              <input
                className="form-control border border-primary"
                type="text"
                onChange={(e) => {
                  filterItems("itemDescription", e.target, items);
                }}
              />
              Item Description
            </th>
            <th scope="col">Purchase Price</th>
            <th scope="col">Quantity</th>
            <th scope="col">Modified By</th>
            <th scope="col">Modified Date</th>
          </tr>
        </thead>

        <tbody>
          {itemsState.map((item, index) => (
            <tr key={index} data-index={index}>
              <td scope="row">{item.purchaseDetailItemAutoId}</td>
              <td scope="row">{item.purchaseOrderNumber}</td>
              <td scope="row">{item.lineNumber}</td>
              <td scope="row">{item.itemNumber}</td>
              <td scope="row">{item.itemName}</td>
              <td scope="row">{item.itemDescription}</td>
              <td scope="row">{formatCurrency(item?.purchasePrice ?? 0)}</td>
              <td scope="row">{item.purchaseQuantity}</td>
              <td scope="row">
                <span className="h6">
                  {item.lastModifiedByUser?.toUpperCase()}
                </span>
              </td>
              <td scope="row">
                <time className="h6">
                  {formatDateTime(item?.lastModifiedDateTime)}
                </time>
              </td>
              <td scope="row">
                <div className="container">
                  <div className="row">
                    <div className="col">
                      <span className="btn-del text-danger">
                        <FontAwesomeIcon
                          icon={"trash"}
                          onClick={(e) => {
                            handleShow(
                              PurchaseActionConstants.DELETE_PURCHASE_ACTION,
                              "Delete Purchase",
                              item
                            );
                          }}
                        ></FontAwesomeIcon>
                      </span>
                    </div>

                    <div className="col">
                      <span className="btn-del text-primary">
                        <FontAwesomeIcon
                          icon={"edit"}
                          onClick={(e) => {
                            handleShow(
                              PurchaseActionConstants.EDIT_PURCHASE_ACTION,
                              "Update Purchase",
                              item
                            );
                          }}
                        ></FontAwesomeIcon>
                      </span>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      {/* Todo: table should be scrollable */}
      <div className="container-fluid">
        <div className="row">
          <div className="col col-md-12">
            <button
              className="btn btn-primary clear-btn"
              onClick={(e) => {
                resetState(items);
              }}
            >
              <FontAwesomeIcon icon={"cancel"}></FontAwesomeIcon>&nbsp;Clear
              filters
            </button>

            <button
              className="btn btn-success add-btn"
              onClick={(e) => {
                handleShow(
                  PurchaseActionConstants.ADD_PURCHASE_ACTION,
                  "Add Purchase"
                );
              }}
            >
              <FontAwesomeIcon icon={"add"}></FontAwesomeIcon>&nbsp;Add Purchase
            </button>
          </div>
        </div>
        <br></br>
        <div className="row">
          <div className="col col-md-12">
            <AlertMessage
              show={alertState.show}
              message={alertState.message}
              variant={alertState.variant}
              onClose={() => {
                setAlertState({
                  show: false,
                  variant: "",
                  message: "",
                });
              }}
            ></AlertMessage>
          </div>
        </div>
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        enforceFocus={true}
        centered={true}
        backdrop="static"
      >
        <Modal.Header closeButton>
          <Modal.Title>{modalTitleState}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <PurchaseActions
            action={actionState}
            item={editItemState}
            onSave={async (
              completedAcion,
              completedMessage,
              completedSuccess
            ) => {
              setShow(false);

              setAlertState({
                show: true,
                variant: completedSuccess ? "success" : "danger",
                message: completedMessage,
              });
              setItemsState(await getPurchaseDetailItems());
            }}
          ></PurchaseActions>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
