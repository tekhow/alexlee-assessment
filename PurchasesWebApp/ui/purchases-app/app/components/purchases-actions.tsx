import {
  Button,
  ButtonGroup,
  Card,
  Col,
  Container,
  Form,
  FormGroup,
  InputGroup,
  Row,
} from "react-bootstrap";
import { PurchaseActionConstants } from "../const/purchase-action-constants";
import {
  PurchaseDetailItem,
  PurchaseDetailItemModified,
  PurchaseDetailItemNew,
} from "../models/purchase-detail-item";
import {
  addPurchaseDetailItem,
  deletePurchaseDetailItem,
  updatePurchaseDetailItem,
} from "../data/purchase-data";
import React from "react";

export default function PurchaseActions({
  action,
  onSave,
  item,
}: {
  action: string;
  onSave: (action: string, message: string, success: boolean) => void;
  item?: PurchaseDetailItem;
}) {
  const [formState, setFormState] = React.useState({
    purchaseOrderNumber: item?.purchaseOrderNumber as string,
    itemNumber: item?.itemNumber as number,
    itemName: item?.itemName as string,
    itemDescription: item?.itemDescription as string,
    purchasePrice: item?.purchasePrice as number,
    purchaseQuantity: item?.purchaseQuantity as number,
  });

  function getItemNameValidationState() {
    const length = formState.itemName.length;
    if (length > 2) return "success";
    if (length < 2) return "error";
    return null;
  }

  switch (action.toUpperCase()) {
    case PurchaseActionConstants.ADD_PURCHASE_ACTION: {
      return (
        <main>
          {/* Todo: add validation */}
          <Card body className="text-disabled">
            <strong>Note: No input validation currently</strong>
          </Card>
          <br></br>
          <Form>
            <Form.Group>
              <Form.Label>Purchase Order Number:</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter purchase order number"
                value={formState.purchaseOrderNumber}
                onChange={(e) => {
                  setFormState({
                    ...formState,
                    purchaseOrderNumber: e.target.value,
                  });
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Item Number:</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter item number"
                value={formState.itemNumber}
                onChange={(e) => {
                  setFormState({
                    ...formState,
                    itemNumber: +e.target.value,
                  });
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Item Name:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter item name"
                value={formState.itemName}
                onChange={(e) => {
                  setFormState({
                    ...formState,
                    itemName: e.target.value,
                  });
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Item Description:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter item description"
                value={formState.itemDescription}
                onChange={(e) => {
                  setFormState({
                    ...formState,
                    itemDescription: e.target.value,
                  });
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Item Price:</Form.Label>
              <InputGroup className="mb-3">
                <InputGroup.Text>$</InputGroup.Text>
                <Form.Control
                  aria-label="Purchase Price"
                  value={formState.purchasePrice}
                  onChange={(e) => {
                    setFormState({
                      ...formState,
                      purchasePrice: +e.target.value,
                    });
                  }}
                />
              </InputGroup>
            </Form.Group>
            <Form.Group>
              <Form.Label>Purchase Quantity:</Form.Label>
              <Form.Control
                type="number"
                value={formState.purchaseQuantity}
                placeholder="Enter purchase quantity"
                onChange={(e) => {
                  setFormState({
                    ...formState,
                    purchaseQuantity: +e.target.value,
                  });
                }}
              />
            </Form.Group>
          </Form>
          <br></br>
          <Container className="container">
            <Row>
              <Button
                variant="primary"
                onClick={async (e) => {
                  if (!formState) {
                    onSave(action, "Invalid item specified", false);
                    return;
                  }

                  let newItem: PurchaseDetailItemNew = {
                    purchaseOrderNumber: formState.purchaseOrderNumber,
                    itemNumber: formState.itemNumber,
                    itemName: formState.itemName,
                    itemDescription: formState.itemDescription,
                    purchasePrice: formState.purchasePrice,
                    purchaseQuantity: formState.purchaseQuantity,
                  };

                  const added = await addPurchaseDetailItem(newItem);
                  const msg = added
                    ? "Item added successfully"
                    : "Item not added";
                  onSave(action, msg, added);
                }}
              >
                Add Purchase Item
              </Button>
            </Row>
          </Container>
        </main>
      );
    }
    case PurchaseActionConstants.EDIT_PURCHASE_ACTION: {
      return (
        <main>
          {/* Todo: add validation */}
          <Card body className="text-disabled">
            <strong>Note: No input validation currently</strong>
          </Card>
          <br></br>
          <Form>
            <Form.Group>
              <Form.Label>Purchase Order Number:</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter purchase order number"
                value={formState.purchaseOrderNumber}
                onChange={(e) => {
                  setFormState({
                    ...formState,
                    purchaseOrderNumber: e.target.value,
                  });
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Item Number:</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter item number"
                value={formState.itemNumber}
                onChange={(e) => {
                  setFormState({
                    ...formState,
                    itemNumber: +e.target.value,
                  });
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Item Name:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter item name"
                value={formState.itemName}
                onChange={(e) => {
                  setFormState({
                    ...formState,
                    itemName: e.target.value,
                  });
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Item Description:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter item description"
                value={formState.itemDescription}
                onChange={(e) => {
                  setFormState({
                    ...formState,
                    itemDescription: e.target.value,
                  });
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Item Price:</Form.Label>
              <InputGroup className="mb-3">
                <InputGroup.Text>$</InputGroup.Text>
                <Form.Control
                  aria-label="Purchase Price"
                  value={formState.purchasePrice}
                  onChange={(e) => {
                    setFormState({
                      ...formState,
                      purchasePrice: +e.target.value,
                    });
                  }}
                />
              </InputGroup>
            </Form.Group>
            <Form.Group>
              <Form.Label>Purchase Quantity:</Form.Label>
              <Form.Control
                type="number"
                value={formState.purchaseQuantity}
                placeholder="Enter purchase quantity"
                onChange={(e) => {
                  setFormState({
                    ...formState,
                    purchaseQuantity: +e.target.value,
                  });
                }}
              />
            </Form.Group>
          </Form>
          <br></br>
          <Container className="container">
            <Row>
              <Button
                variant="primary"
                onClick={async (e) => {
                  if (!item) {
                    onSave(action, "Invalid item specified", false);
                    return;
                  }

                  const updatedItem: PurchaseDetailItemModified = {
                    purchaseDetailItemAutoId: item?.purchaseDetailItemAutoId,
                    purchaseOrderNumber: formState?.purchaseOrderNumber,
                    itemNumber: formState?.itemNumber,
                    itemName: formState?.itemName,
                    itemDescription: formState?.itemDescription,
                    purchasePrice: formState?.purchasePrice,
                    purchaseQuantity: formState?.purchaseQuantity,
                  };

                  console.log(updatedItem);
                  const added = await updatePurchaseDetailItem(updatedItem);
                  const msg = added
                    ? "Item updated successfully"
                    : "Item not updated";
                  onSave(action, msg, added);
                }}
              >
                Update Purchase Item
              </Button>
            </Row>
          </Container>
        </main>
      );
    }
    case PurchaseActionConstants.DELETE_PURCHASE_ACTION: {
      return (
        <main>
          <Container className="container">
            <Row>
              <Button
                variant="primary"
                onClick={async (e) => {
                  if (!item) {
                    onSave(action, "Invalid item specified", false);
                    return;
                  }

                  const added = await deletePurchaseDetailItem(
                    item.purchaseDetailItemAutoId
                  );
                  const msg = added
                    ? "Item delete successfully"
                    : "Item not deleted";
                  onSave(action, msg, added);
                }}
              >
                Delete Purchase Item
              </Button>
            </Row>
          </Container>
        </main>
      );
    }
  }
}
