import { useState } from "react";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";

function AlertMessage({ show,variant, message, onClose }: {show:boolean,variant:string, message:string, onClose:() => void}) {

    return (
      <Alert variant={variant} onClose={onClose} show={show} dismissible>
        <Alert.Heading>Message</Alert.Heading>
        <p>
          {message}
        </p>
      </Alert>
    );
  }

export default AlertMessage;
