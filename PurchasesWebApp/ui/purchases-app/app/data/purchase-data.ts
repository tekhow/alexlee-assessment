import { PurchaseDetailItem, PurchaseDetailItemModified, PurchaseDetailItemNew } from "../models/purchase-detail-item";

//Todo: make env setting
const purchaseBaseurl = "http://localhost:5162/purchases"; 

/**
 * Adds a new purchase detail item to the backend API.
 * @param {PurchaseDetailItem} item - The purchase detail item to add
 * @returns {boolean} - Whether the addition was successful or not
 */
export const addPurchaseDetailItem = async (item: PurchaseDetailItemNew): Promise<boolean> => {
    const res = await fetch(purchaseBaseurl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(item),
    });
  
  return res.status === 201;
};
/**
 * Fetches all purchase detail items from the backend API and returns them as an array of PurchaseDetailItem objects.
 * @returns {Promise<PurchaseDetailItem[]>} An array of purchase detail items
 */
export const getPurchaseDetailItems = async (): Promise<
  PurchaseDetailItem[]
> => {
  const res = await fetch(purchaseBaseurl);
  const purchaseDetailItems: PurchaseDetailItem[] = await res.json();
  return purchaseDetailItems;
};

/**
 * Deletes a purchase detail item by its ID
 * @param {number} id - The ID of the purchase detail item to delete
 * @returns {boolean} - Whether the deletion was successful or not
 */
export const deletePurchaseDetailItem = async (
  id: number
): Promise<boolean> => {
  const res = await fetch(`${purchaseBaseurl}?id=${id}`, {
    method: "DELETE",
    
  });
console.log(res);
  return res.ok;
};

/**
 * Updates an existing purchase detail item in the backend API.
 * @param {PurchaseDetailItem} item - The updated purchase detail item
 * @returns {boolean} - Whether the update was successful or not
 */
export const updatePurchaseDetailItem = async (
  item: PurchaseDetailItemModified
): Promise<boolean> => {
  const res = await fetch(purchaseBaseurl, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(item),
    });
  
  return res.status === 202;
};