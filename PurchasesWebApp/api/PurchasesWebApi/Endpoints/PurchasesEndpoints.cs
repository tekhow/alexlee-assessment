using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PurchasesWebApi.Data;
using PurchasesWebApi.Model;
using PurchasesWebApi.Repository;
using PurchasesWebApp.Repository;

namespace PurchasesWebApi.Endpoints
{
    public static class PurchasesEndpoints
    {

        /// <summary>
        /// Adds the purchase endpoints.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>The RouteHandlerBuilder.</returns>
        public static RouteHandlerBuilder AddPurchaseEndpoints(this WebApplication app)
        {
            app.AddPurchasesEndpoint();
            app.AddGetPurchasesEndpoint();
            app.AddDeletePurchaseEndpoint();
            return app.AddUpdatePurchasesEndpoint();
        }

        /// <summary>
        /// Adds the purchases endpoint.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>The RouteHandlerBuilder.</returns>
        private static RouteHandlerBuilder AddPurchasesEndpoint(this WebApplication app)
        {
            return app.MapPost("/purchases", async ([FromBody] PurchaseDetailItem item, IPurchaseRepository repo) =>
            {
                var added = await repo.AddPurchaseItemAsync(item);

                return added ? Results.Created() : Results.NotFound();
            })
            .WithName("AddPurchases");
        }

        /// <summary>
        /// Adds the get purchases endpoint.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>The RouteHandlerBuilder.</returns>
        private static RouteHandlerBuilder AddGetPurchasesEndpoint(this WebApplication app)
        {
            return app.MapGet("/purchases", async (IPurchaseRepository repo) =>
            {
                var purchases = await repo.GetPurchaseItemsAsync().ToListAsync();

                return Results.Ok(purchases);
            })
            .WithName("GetPurchases");
        }

        /// <summary>
        /// Adds the delete purchase endpoint.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>The RouteHandlerBuilder.</returns>
        private static RouteHandlerBuilder AddDeletePurchaseEndpoint(this WebApplication app)
        {
            return app.MapDelete("/purchases", async ([FromQuery] long id, IPurchaseRepository repo) =>
            {
                var result = await repo.DeletePurchaseItemAsync(id);

                return result ? Results.Ok() : Results.NotFound();
            })
            .WithName("DeletePurchase");
        }

        /// <summary>
        /// Adds the update purchases endpoint.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>The RouteHandlerBuilder.</returns>
        private static RouteHandlerBuilder AddUpdatePurchasesEndpoint(this WebApplication app)
        {
            return app.MapPut("/purchases", async ([FromBody] PurchaseDetailItem item, IPurchaseRepository repo) =>
            {
                var updated = await repo.UpdatePurchaseItemAsync(item);

                return updated ? Results.Accepted() : Results.NotFound();
            })
            .WithName("UpdatePurchases");
        }
    }
}