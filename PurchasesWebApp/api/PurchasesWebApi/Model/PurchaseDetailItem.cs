using System.ComponentModel.DataAnnotations.Schema;

namespace PurchasesWebApi.Model
{
    public class PurchaseDetailItem
    {
        public long PurchaseDetailItemAutoId { get; set; } = 0;
        public string PurchaseOrderNumber { get; set; } = "";
        public long LineNumber { get; set; } = 0;
        public int ItemNumber { get; set; } = 0;
        public string ItemName { get; set; } = "";
        public string ItemDescription { get; set; } = "";
        public decimal PurchasePrice { get; set; } = 0.0m;
        public int PurchaseQuantity { get; set; } = 0;
        public string? LastModifiedByUser { get; set; } = "";
        public System.DateTime? LastModifiedDateTime { get; set; }
    }
}