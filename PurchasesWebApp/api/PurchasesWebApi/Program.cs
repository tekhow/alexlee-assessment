
using Microsoft.EntityFrameworkCore;
using PurchasesWebApi.Data;
using PurchasesWebApi.Endpoints;
using PurchasesWebApi.Repository;
using PurchasesWebApp.Repository;

const string CORS_POLICY_NAME = "__CorsPolicy";

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddCors(setup =>
{
    setup.AddPolicy(CORS_POLICY_NAME, builder =>
    builder
    .AllowAnyOrigin().
    AllowAnyHeader()
    .AllowAnyMethod());
});


builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<PurchasesDbContext>(options =>
{
    options.EnableSensitiveDataLogging();
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});
builder.Services.AddScoped<IPurchaseRepository, PurchaseRepository>();


var app = builder.Build();
app.UseCors(CORS_POLICY_NAME);
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.AddPurchaseEndpoints();
app.Run();

