using PurchasesWebApi.Model;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
namespace PurchasesWebApi.Data
{
    public class PurchasesDbContext : DbContext
    {
        public DbSet<PurchaseDetailItem> PurchaseDetailItem => Set<PurchaseDetailItem>();

        public PurchasesDbContext(DbContextOptions<PurchasesDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}