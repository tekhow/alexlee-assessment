using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PurchasesWebApi.Data;
using PurchasesWebApi.Model;
using PurchasesWebApi.Repository;

namespace PurchasesWebApp.Repository
{
    public class PurchaseRepository(PurchasesDbContext Context) : IPurchaseRepository
    {
        public async Task<bool> AddPurchaseItemAsync(PurchaseDetailItem item)
        {
            item.LastModifiedDateTime = DateTime.UtcNow;
            item.LastModifiedByUser = Environment.MachineName.ToUpper();
            await Context.PurchaseDetailItem.AddAsync(item);
            var added = await Context.SaveChangesAsync();
            return added > 0;
        }

        public async Task<bool> DeletePurchaseItemAsync(long id)
        {
            var item = new PurchaseDetailItem { PurchaseDetailItemAutoId = id };
            Context.PurchaseDetailItem.Attach(item);
            Context.PurchaseDetailItem.Remove(item);
            var deleted = await Context.SaveChangesAsync();
            return deleted > 0;
        }

        public IQueryable<PurchaseDetailItem> GetPurchaseItemsAsync()
        {
            var result = Context.PurchaseDetailItem.FromSqlRaw($"EXEC dbo.p_SelectPurchaseDetailItemWithLineNumber");

            return result;
        }

        public async Task<bool> UpdatePurchaseItemAsync(PurchaseDetailItem item)
        {
            item.LastModifiedDateTime = DateTime.UtcNow;
            item.LastModifiedByUser = Environment.MachineName.ToUpper();

            var found = await Context.PurchaseDetailItem.FirstAsync(v => v.PurchaseDetailItemAutoId == item.PurchaseDetailItemAutoId);
            if (found == null)
            {
                throw new Exception("Item not found");
            }

            Context.Entry(found).CurrentValues.SetValues(item);

            var updated = await Context.SaveChangesAsync();
            return updated > 0;
        }
    }
}