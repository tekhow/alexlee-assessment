using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PurchasesWebApi.Model;

namespace PurchasesWebApi.Repository
{
    public interface IPurchaseRepository
    {
        IQueryable<PurchaseDetailItem> GetPurchaseItemsAsync();

        Task<bool> DeletePurchaseItemAsync(long id);

        Task<bool> AddPurchaseItemAsync(PurchaseDetailItem item);

        Task<bool> UpdatePurchaseItemAsync(PurchaseDetailItem item);
    }
}