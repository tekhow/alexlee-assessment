using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PurchasesWebApi.Model;

namespace PurchasesWebApi.Configuration
{
    public class PurchasesDbConfiguration : IEntityTypeConfiguration<PurchaseDetailItem>
    {
        public void Configure(EntityTypeBuilder<PurchaseDetailItem> builder)
        {
            //builder.ToTable("PurchaseDetailItem");
            builder
                .HasKey(o => o.PurchaseDetailItemAutoId);
            builder.Property(p => p.PurchasePrice)
                .HasColumnType("decimal(10,2)");
            builder.Property(p => p.LineNumber)
              .HasColumnType("bigint");
            builder.Ignore(c => c.LineNumber);

        }
    }
}