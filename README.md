# AlexLee Assessment

* #### Taker: Corey Howard
* #### Contact: 251.610.4629 - coreyh@tekhow.net

## 1 & 2. Unit tests

- Located in UnitTests folder and UnitTests1.cs

## 3. File searcher Console application

- Located in FileSearcherConsole folder
- Set search path in application.json file

  ```json
  {
    "AppSettings": {
        "DirectorySearchPath": "./bin/Debug/net8.0/TestFiles"
     }
  }

- Value to search for can be set in application.json file

  ```json
  {
  "AppSettings": {
    "SearchTerm":"technologies"
  }

 }

- Output should look like this
![Alt text](./.markdown/console.png)

## 4, 5, & 6. SQL

- Located in SQL folder

## 7 & 8. React\Nextjs UI

- Located in the PurchasesWebApp folder
- /api folder container the .Net8 minimal apis and EFCore - start with __dotnet watch run__
  - Should see a swagger endpoint like below
  ![Alt text](./.markdown/swagger.png)
- /ui is the React\Nextjs application - start with __npm run dev__
  - Should see UI render like below
  ![Alt text](./.markdown/ui.png)