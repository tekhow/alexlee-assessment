using System.Diagnostics;
using TestMethods;
namespace alexlee_assessment;

[TestClass]
public class UnitTest1
{

    [TestInitialize]
    public void TestInit()
    {

    }

    [TestMethod]
    public void CreateInterleavedStringTest()
    {
        var result = Methods.CreateInterleavedString("abc", "1234");
        Assert.AreEqual("a1b2c34", result);
    }

    [TestMethod]
    public void CheckIsStringPalendromeTest()
    {
        Assert.AreEqual("Palendrome", Methods.CheckIsStringPalendrome("madam"));

        Assert.AreEqual("Palendrome", Methods.CheckIsStringPalendrome("step on no pets"));

        Assert.AreEqual("Not Palendrome", Methods.CheckIsStringPalendrome("book"));

        Assert.AreEqual("Palendrome", Methods.CheckIsStringPalendrome(1221));
    }
}